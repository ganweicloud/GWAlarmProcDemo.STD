# 服务端协议开发示例

### 介绍
服务端报警协议开发示例Demo

### 软件架构

![img.png](./doc/img.png)

### 安装教程

将生成的文件复制到软件目录的dll下
![img.png](img.png)


AlarmProc表配置
| **字段** | **类型**  | **描述**        |
|:------:|---------|---------------|
|   Proc_Code    | 主键      | 主键            |
|   Proc_Module    | 驱动模块文件名 | 报警协议文件名， 此处为：GWAlarmProcDemo.STD.dll |
|   Proc_name    | 驱动名称    | 名称            |
|   Proc_parm    | 驱动启动参数  | 参数配置：自行定义，在报警dll中解析。本示例的参数配置（使用腾讯企业邮箱）：mail=g*****@g****.com;pwd=*****;title=报警插件测试<br/> mail：邮箱名<br/> pwd：邮箱密码<br/> title：邮件主题           |


### 使用说明


#### 初始化配置

本例程代码里需要用到邮箱发送邮件，需自提供邮箱参数。init用于初始化邮件配置

```csharp
public override bool init(AlarmProcTableRow row)
{
    try
    {
        if (MyEmail == null)
            MyEmail = new SendEmail();
                
        strParm = DataCenter.AESDecrypt(row.Proc_parm, DataCenter.GeneratMD5Key());
        moduleNm = row.Proc_name;
        if (strParm.IndexOf(';') > 0)
        {
            MailAddress = strParm.Split(';')[0];
            if (MailAddress.IndexOf('=') > 0)
            {
                MailAddress = MailAddress.Split('=')[1].Trim();
            }
            PassWord = strParm.Split(';')[1];
            if (PassWord.IndexOf('=') > 0)
            {
                PassWord = PassWord.Split('=')[1].Trim();
            }
            Title = strParm.Split(';')[2];
            if (Title.IndexOf('=') > 0)
            {
                Title = Title.Split('=')[1].Trim();
            }
            StartAlarmThread();
        }
    }
    catch (Exception e)
    {
        MessageService.AddMessage(MessageLevel.Fatal, General.GetExceptionInfo(e), 0, false);
    }
    return true;
}
```

#### 报警发生时的处理

```csharp
public override bool DoAlarm(CAlarmItem item)
{
    var dt = item.GetAdminsOfAlarm(item.Equipno, item.Time);//得到报警人员列表
    foreach (var r in dt)
    {
        try
        {
            var level = r.AckLevel.GetType() == typeof(System.DBNull) ? 0 : r.AckLevel;
            if (level == item.AlarmLevel)//同一级别才报警
            {
                string content = item.Time.ToString("HH:mm:ss ") + item.Event;
                //数据库里面的Email地址已经加密，需要解密
                string strEmail = DataCenter.AESDecrypt(r.EMail, DataCenter.GeneratMD5Key());
                if (!MyEmail.Init(MailAddress, PassWord, strEmail, Title))
                {
                    string msg1 = string.Format("{0}:{1}--{2}--{3}", strEmail,
                    ResourceService.GetString("AlarmCenter.Email.Msg1", "发送Email报警"),
                    item.Event,
                    ResourceService.GetString("AlarmCenter.Email.Msg3", "失败!"));
                    MessageService.AddMessage(MessageLevel.Info, msg1, 0, false);

                    lock (GWDbProvider.lockstate)
                    {
                        using var db = StationItem.MyGWDbProvider.serviceProvider.GetService<GWDataContext>();
                        var row = new AlarmRecTableRow
                        {
                            proc_name = moduleNm,
                            Administrator = r.Administrator,
                            gwEvent = item.Event,
                            time = General.Convert2DT(DateTime.Now),
                            comment = ResourceService.GetString("AlarmCenter.Email.Msg3")
                        };
                        db.AlarmRecTable.Add(row);
                        db.SaveChanges();
                    }
                    continue;
                }
                MyEmail.SendBodyTxt(content);//设置发邮件内容
                MyEmail.Send();
                string msg = string.Format("{0}:{1}--{2}--{3}", strEmail,
                ResourceService.GetString("AlarmCenter.Email.Msg1", "发送Email报警"),
                item.Event,
                ResourceService.GetString("AlarmCenter.Email.Msg2", "成功!"));
                MessageService.AddMessage(MessageLevel.Info, msg, 0);

                lock (GWDbProvider.lockstate)
                {
                    using var db = StationItem.MyGWDbProvider.serviceProvider.GetService<GWDataContext>();
                    var row = new AlarmRecTableRow
                    {
                        proc_name = moduleNm,
                        Administrator = r.Administrator,
                        gwEvent = item.Event,
                        time = General.Convert2DT(DateTime.Now),
                        comment = "报警成功"
                    };
                    db.AlarmRecTable.Add(row);
                    db.SaveChanges();
                }
            }
        }
        catch (Exception e)
        {
            DataCenter.WriteLogFile(General.GetExceptionInfo(e));
            Thread.Sleep(5000);
            MyEmail = new SendEmail();
            MessageService.AddMessage(MessageLevel.Warn, General.GetExceptionInfo(e), 0, false);
        }
    }

    return true;
}
```