﻿using GWDataCenter;
using GWDataCenter.Database;
using Microsoft.Extensions.DependencyInjection;

namespace GWAlarmProcDemo.STD
{
    public class CAlarm : CAlarmEquipBase
    {
        Thread AlarmScanThread;
        SendEmail MyEmail = null;
        string strParm;
        string moduleNm;
        string MailAddress, PassWord, Title;//邮件参数
        CAlarmItem CurrentAlarmItem = null;
        public void StartAlarmThread()
        {
            ThreadStart entryPoint = new ThreadStart(AlarmScan);
            AlarmScanThread = new Thread(entryPoint);
            AlarmScanThread.IsBackground = true;
            AlarmScanThread.Start();
        }

        private void AlarmScan()
        {
            while (true)
            {
                if (CurrentAlarmItem != null)
                {
                    DoAlarm(CurrentAlarmItem);
                    CurrentAlarmItem = null;
                }

                lock (AlarmItemQueue)
                {
                    if (AlarmItemQueue.Count > 0)
                    {
                        CurrentAlarmItem = (CAlarmItem)AlarmItemQueue.Dequeue();
                    }
                }
                ThreadPool.QueueUserWorkItem(new WaitCallback(CheckRise));
                Thread.Sleep(500);
            }
        }

        //每隔一秒钟检查延时报警列表
        void CheckRise(object o)
        {
            Thread.Sleep(1000);
            CheckAlarmRise();
        }

        public override bool init(AlarmProcTableRow row)
        {
            try
            {
                if (MyEmail == null)
                    MyEmail = new SendEmail();
                
                // 由于
                strParm = DataCenter.AESDecrypt(row.Proc_parm, DataCenter.GeneratMD5Key());
                moduleNm = row.Proc_name;
                if (strParm.IndexOf(';') > 0)
                {
                    MailAddress = strParm.Split(';')[0];
                    if (MailAddress.IndexOf('=') > 0)
                    {
                        MailAddress = MailAddress.Split('=')[1].Trim();
                    }
                    PassWord = strParm.Split(';')[1];
                    if (PassWord.IndexOf('=') > 0)
                    {
                        PassWord = PassWord.Split('=')[1].Trim();
                    }
                    Title = strParm.Split(';')[2];
                    if (Title.IndexOf('=') > 0)
                    {
                        Title = Title.Split('=')[1].Trim();
                    }
                    StartAlarmThread();
                }
            }
            catch (Exception e)
            {
                MessageService.AddMessage(MessageLevel.Fatal, General.GetExceptionInfo(e), 0, false);
            }
            return true;
        }
        public override bool DoAlarm(CAlarmItem item)
        {
            var dt = item.GetAdminsOfAlarm(item.Equipno, item.Time);//得到报警人员列表
            foreach (var r in dt)
            {
                try
                {
                    var level = r.AckLevel.GetType() == typeof(System.DBNull) ? 0 : r.AckLevel;
                    if (level == item.AlarmLevel)//同一级别才报警
                    {
                        string content = item.Time.ToString("HH:mm:ss ") + item.Event;
                        //数据库里面的Email地址已经加密，需要解密
                        string strEmail = DataCenter.AESDecrypt(r.EMail, DataCenter.GeneratMD5Key());
                        if (!MyEmail.Init(MailAddress, PassWord, strEmail, Title))
                        {
                            string msg1 = string.Format("{0}:{1}--{2}--{3}", strEmail,
                            ResourceService.GetString("AlarmCenter.Email.Msg1", "发送Email报警"),
                            item.Event,
                            ResourceService.GetString("AlarmCenter.Email.Msg3", "失败!"));
                            MessageService.AddMessage(MessageLevel.Info, msg1, 0, false);

                            lock (GWDbProvider.lockstate)
                            {
                                using var db = StationItem.MyGWDbProvider.serviceProvider.GetService<GWDataContext>();
                                var row = new AlarmRecTableRow
                                {
                                    proc_name = moduleNm,
                                    Administrator = r.Administrator,
                                    gwEvent = item.Event,
                                    time = General.Convert2DT(DateTime.Now),
                                    comment = ResourceService.GetString("AlarmCenter.Email.Msg3")
                                };
                                db.AlarmRecTable.Add(row);
                                db.SaveChanges();
                            }
                            continue;
                        }
                        MyEmail.SendBodyTxt(content);//设置发邮件内容
                        MyEmail.Send();
                        string msg = string.Format("{0}:{1}--{2}--{3}", strEmail,
                        ResourceService.GetString("AlarmCenter.Email.Msg1", "发送Email报警"),
                        item.Event,
                        ResourceService.GetString("AlarmCenter.Email.Msg2", "成功!"));
                        MessageService.AddMessage(MessageLevel.Info, msg, 0);

                        lock (GWDbProvider.lockstate)
                        {
                            using var db = StationItem.MyGWDbProvider.serviceProvider.GetService<GWDataContext>();
                            var row = new AlarmRecTableRow
                            {
                                proc_name = moduleNm,
                                Administrator = r.Administrator,
                                gwEvent = item.Event,
                                time = General.Convert2DT(DateTime.Now),
                                comment = "报警成功"
                            };
                            db.AlarmRecTable.Add(row);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception e)
                {
                    DataCenter.WriteLogFile(General.GetExceptionInfo(e));
                    Thread.Sleep(5000);
                    MyEmail = null;
                    MyEmail = new SendEmail();
                    MessageService.AddMessage(MessageLevel.Warn, General.GetExceptionInfo(e), 0, false);
                }
            }

            return true;
        }
    }
}