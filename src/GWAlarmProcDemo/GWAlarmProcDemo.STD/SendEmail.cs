﻿using System.Net;
using System.Net.Mail;
using System.Text;

namespace GWAlarmProcDemo.STD
{
    public  class SendEmail
    {
        private readonly SmtpClient _smtpClient = new ();
        private readonly MailMessage _mailMessage = new ();

        public bool Init(string mailAddress, string password, string strEmail, string title)
        {
            _smtpClient.Host = "smtp.exmail.qq.com";//qq企业邮箱
            //使用安全加密连接。
            _smtpClient.EnableSsl = true;
            //不和请求一块发送。
            _smtpClient.UseDefaultCredentials = false;

            //验证发件人身份(发件人的邮箱&#xff0c;邮箱里的生成授权码);
            _smtpClient.Credentials = new NetworkCredential(mailAddress, password);

            _mailMessage.Priority = MailPriority.Normal;

            //发件人邮箱地址。
            _mailMessage.From = new MailAddress(mailAddress, "敢为云");
            _mailMessage.To.Add(new MailAddress(strEmail)); //收件人邮箱地址。
            _mailMessage.SubjectEncoding = Encoding.GetEncoding(936);

            //邮件正文是否是HTML格式
            _mailMessage.IsBodyHtml = false;

            //邮件标题。
            _mailMessage.Subject = title;

            return true;
        }

        public void SendBodyTxt(string content)
        {
            //邮件内容。
            _mailMessage.Body = content;
        }

        public void Send()
        {
            _smtpClient.Send(_mailMessage);
        }

    }
}
